=======
skutils
=======


.. image:: https://img.shields.io/pypi/v/skutils.svg
        :target: https://pypi.python.org/pypi/skutils

.. image:: https://img.shields.io/travis/sbneto/skutils.svg
        :target: https://travis-ci.org/sbneto/skutils

.. image:: https://readthedocs.org/projects/skutils/badge/?version=latest
        :target: https://skutils.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Some scikit utilities


* Free software: MIT license
* Documentation: https://skutils.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
