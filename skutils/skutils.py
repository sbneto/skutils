# -*- coding: utf-8 -*-
import logging
from functools import partial

import numpy as np
from sklearn import tree
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import cross_val_predict, KFold, cross_validate
from sklearn.metrics import confusion_matrix
from sklearn import metrics
from hyperopt import fmin, tpe, Trials, space_eval

# plotting auc libs
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score


logger = logging.getLogger(__name__)


# http://scikit-learn.org/stable/auto_examples/hetero_feature_union.html#sphx-glr-auto-examples-hetero-feature-union-py
class ItemSelector(BaseEstimator, TransformerMixin):
    """For data grouped by feature, select subset of data at a provided key.

    The data is expected to be stored in a 2D data structure, where the first
    index is over features and the second is over samples.  i.e.

    >> len(data[key]) == n_samples

    Please note that this is the opposite convention to scikit-learn feature
    matrixes (where the first index corresponds to sample).

    ItemSelector only requires that the collection implement getitem
    (data[key]).  Examples include: a dict of lists, 2D numpy array, Pandas
    DataFrame, numpy record array, etc.

    >> data = {'a': [1, 5, 2, 5, 2, 8],
               'b': [9, 4, 1, 4, 1, 3]}
    >> ds = ItemSelector(key='a')
    >> data['a'] == ds.transform(data)

    ItemSelector is not designed to handle data grouped by sample.  (e.g. a
    list of dicts).  If your data is structured this way, consider a
    transformer along the lines of `sklearn.feature_extraction.DictVectorizer`.

    Parameters
    ----------
    key : hashable, required
        The key corresponding to the desired value in a mappable.
    """
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key].to_numpy().reshape(-1, 1)


class Reshape(BaseEstimator, TransformerMixin):
    def __init__(self, shape1, shape2):
        self.shape1 = shape1
        self.shape2 = shape2

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x.reshape(self.shape1, self.shape2)


class Cast(BaseEstimator, TransformerMixin):
    def __init__(self, cast_type):
        self.cast_type = cast_type

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x.astype(self.cast_type)


def fix_pipeline_transform(klass):
    class _FixedTransformer(klass):
        def fit(self, x, y=None):
            """this would allow us to fit the model based on the X input."""
            super(_FixedTransformer, self).fit(x)

        def transform(self, x, y=None):
            return super(_FixedTransformer, self).transform(x)

        def fit_transform(self, x, y=None):
            return super(_FixedTransformer, self).fit(x).transform(x)
    return _FixedTransformer


# https://github.com/hyperopt/hyperopt
class ParametersHelper:
    def __init__(self, search_space, classifier_class=tree.DecisionTreeClassifier,
                 suggest_kwargs=None, folds=10, n_jobs=1, seed=12345, fit_params={},
                 max_evals=60, scoring=None, objective=None, verbosity=True):
        self.classifier_class = classifier_class
        self.suggest_kwargs = suggest_kwargs or dict(
            n_startup_jobs=20, gamma=0.25, n_EI_candidates=24
        )
        self.search_space = search_space

        self.max_evals = max_evals
        self.x = None
        self.y = None
        self.trials = None
        self.best = None
        self.classifier = None
        self.folds = folds
        self.seed = seed
        self.n_jobs = n_jobs
        self.fit_params = fit_params
        self.scoring = scoring
        self.objective = objective or ParametersHelper.binary_objective_function
        self.verbosity = verbosity

    def multiclass_objective_function(self, params):
        logger.info('Evaluating objective function for %s', params)
        estimator = self.classifier_class(**params)
        scores = cross_validate(
            estimator=estimator,
            X=self.x,
            y=self.y,
            cv=self.folds,
            verbose=1,
            scoring=self.scoring or metrics.make_scorer(metrics.roc_auc_score, multi_class='ovo', needs_proba=True),
            n_jobs=self.n_jobs,
            fit_params=self.fit_params,
        )
        result = -1.0 * np.mean(scores['test_score'])
        return result

    def binary_objective_function(self, params):
        """
        Only True predictions are interesting for this objective function.
        """
        logger.info('Evaluating objective function for %s', params)
        scores = cross_validate(
            self.classifier_class(**params),
            self.x,
            self.y,
            # StratifiedKFold was behaving strangely, we were seeing the classifier
            # performance vary with the number of folds in a very drastic way.
            # Changing to KFold solved the problem.
            cv=KFold(
                n_splits=self.folds,
                shuffle=True,
                random_state=np.random.RandomState(seed=self.seed)
            ),
            # Set to 1 to avoid problems with LGBMClassifier
            n_jobs=self.n_jobs,
            scoring=self.scoring or 'roc_auc',
            fit_params=self.fit_params,
        )
        result = -1.0 * np.mean(scores['test_score'])
        return result

    def fit(self, x, y):
        self.x = x
        self.y = y
        self.trials = Trials()
        self.best = fmin(
            self.multiclass_objective_function,
            space=self.search_space,
            algo=partial(tpe.suggest, **self.suggest_kwargs),
            max_evals=self.max_evals,
            trials=self.trials,
            rstate=np.random.RandomState(seed=self.seed),
            verbose=self.verbosity,
        )

    def fit_classifier(self, x=None, y=None):
        self.x = x if x is not None else self.x
        self.y = y if y is not None else self.y
        self.classifier = self.classifier_class(**self.space_eval())
        self.classifier.fit(self.x, self.y, **self.fit_params)

    def space_eval(self):
        return space_eval(self.search_space, self.best) if self.search_space else {}

    def plot_auc(self, x=None, y=None):
        self.x = x if x is not None else self.x
        self.y = y if y is not None else self.y
        self.classifier = self.classifier_class(**self.space_eval())

        y_score = cross_val_predict(
            self.classifier,
            self.x,
            self.y,
            # StratifiedKFold was behaving strangely, we were seeing the classifier
            # performance vary with the number of folds in a very drastic way.
            # Changing to KFold solved the problem.
            cv=KFold(
                n_splits=self.folds,
                shuffle=True,
                random_state=np.random.RandomState(seed=self.seed)
            ),
            # Set to 1 to avoid problems with LGBMClassifier
            n_jobs=self.n_jobs,
            method='predict_proba',
            fit_params=self.fit_params,
        )

        plt.figure()
        lw = 2
        fpr, tpr, _ = roc_curve(self.y, y_score[:, 1])
        plt.plot(fpr, tpr, color='darkorange',
                 lw=lw, label='ROC curve (area = %.2f)' % roc_auc_score(self.y, y_score[:, 1]))
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")
        plt.show()

    def cross_val_confusion_matrix(self, x=None, y=None):
        self.x = x if x is not None else self.x
        self.y = y if y is not None else self.y
        self.classifier = self.classifier_class(**self.space_eval())
        y_pred = cross_val_predict(
            self.classifier,
            self.x,
            self.y,
            # StratifiedKFold was behaving strangely, we were seeing the classifier
            # performance vary with the number of folds in a very drastic way.
            # Changing to KFold solved the problem.
            cv=KFold(
                n_splits=self.folds,
                shuffle=True,
                random_state=np.random.RandomState(seed=self.seed)
            ),
            # Set to 1 to avoid problems with LGBMClassifier
            n_jobs=self.n_jobs,
            fit_params=self.fit_params,
        )
        return confusion_matrix(self.y, y_pred)
