# -*- coding: utf-8 -*-

"""Top-level package for skutils."""

__author__ = """Samuel Barbosa"""
__email__ = 'samuel.m.b.neto@gmail.com'
__version__ = '0.1.0'
