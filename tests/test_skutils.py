#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `skutils` package."""

from sklearn import datasets
import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.utils import resample
from hyperopt import hp
from hyperopt.pyll.base import scope
from skutils.skutils import ItemSelector, fix_pipeline_transform, ParametersHelper
from lightgbm import LGBMClassifier


PipelineLabelEncoder = fix_pipeline_transform(LabelEncoder)


def test_basic_classifier():
    data = datasets.fetch_kddcup99()

    x_raw = pd.DataFrame(data['data'])
    y_raw = pd.DataFrame(data['target'])
    mask = (y_raw[0] == b'neptune.') | (y_raw[0] == b'normal.')
    x_filtered = x_raw[mask].reset_index().infer_objects()
    y_filtered = y_raw[mask].infer_objects()

    def binanizer_pipeline(key, name=None):
        name = name or str(key)
        return (name, Pipeline([
            ('selector', ItemSelector(key=key)),
            ('binarizer', OneHotEncoder(dtype=np.int64, handle_unknown='ignore')),
        ]))

    pipeline = Pipeline([
        ('union', FeatureUnion(
            transformer_list=[
                binanizer_pipeline(1),
                binanizer_pipeline(2),
                binanizer_pipeline(3),
            ] + [(str(col), ItemSelector(col)) for col in [30, 31, 32]],
        )),
    ])

    y = PipelineLabelEncoder().fit_transform(y_filtered)
    x = pipeline.fit_transform(x_filtered)

    # Define searched space
    hyper_space = {
        'max_depth':  scope.int(hp.quniform('max_depth', 3, 33, 5)),
        'min_data_in_leaf': scope.int(hp.quniform('min_data_in_leaf', 30, 50, 10)),
        'n_estimators': scope.int(20 + hp.randint('n_estimators', 20)),
        'num_leaves': scope.int(hp.quniform('num_leaves', 2, 5, 1)),
        'is_unbalance': True,
        'max_bin': scope.int(hp.quniform('max_bin', 2, 5, 1)),
        'boosting': 'gbdt',
        'verbosity': -1,
        'seed': 12345,
    }

    helper = ParametersHelper(
        hyper_space,
        classifier_class=LGBMClassifier,
        folds=3,
        n_jobs=1,
    )
    x_sample, y_sample = resample(x, y, n_samples=10000, random_state=12345)
    helper.fit(x_sample, y_sample)

    assert helper.space_eval() == {
        'boosting': 'gbdt',
        'is_unbalance': True,
        'max_bin': 3,
        'max_depth': 5,
        'min_data_in_leaf': 50,
        'n_estimators': 36,
        'num_leaves': 2,
        'seed': 12345,
        'verbosity': -1,
    }

    scores = [-trial['result']['loss'] for trial in helper.trials.trials]
    assert np.max(scores) == 0.9999992168616408
